package com.example.palantir.service;

import com.example.palantir.models.spells.Spell;
import com.example.palantir.models.spells.SpellList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Component
public class SpellService {
    private static String DND_SPELL_URL = "http://dnd5eapi.co/api/spells/";

    @Autowired
    private RestTemplate restTemplate;

    public SpellList getSpells() {
        URI uri = UriComponentsBuilder
                .fromUriString(DND_SPELL_URL)
                .build()
                .toUri();

        return restTemplate.getForObject(uri, SpellList.class);

    }

    public Spell getSpellDetails(String spellId) {
        URI uri = UriComponentsBuilder
                .fromUriString(DND_SPELL_URL + spellId)
                .build()
                .toUri();

        return restTemplate.getForObject(uri, Spell.class);
    }
}
