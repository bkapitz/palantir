package com.example.palantir.models.spells;

import com.example.palantir.models.util.ReferenceLink;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SpellList implements Serializable {
    private static final long serialVersionUID = -6582140975450232730L;

    private Integer count;
    @JsonProperty("results")
    private List<ReferenceLink> spellList;
}
