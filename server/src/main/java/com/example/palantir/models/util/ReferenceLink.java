package com.example.palantir.models.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferenceLink implements Serializable{
    private static final long serialVersionUID = -4509942482175168759L;

    @NonNull
    private String url;
    @NonNull
    private String name;

    public String getId() {
        return url.substring(url.lastIndexOf("/") + 1).trim();
    }

}
