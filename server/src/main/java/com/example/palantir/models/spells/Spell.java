package com.example.palantir.models.spells;

import com.example.palantir.models.util.ReferenceLink;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Spell implements Serializable {
    private static final long serialVersionUID = 4130122649820488254L;

    private Integer index;
    private String name;
    private ArrayList<String> desc;
    private String higherLevel;
    private String page;
    private String range;
    private ArrayList<String> components;
    private String material;
    private String ritual;
    private String duration;
    private String concentration;
    private String castingTime;
    private Integer level;
    private ReferenceLink school;
    private ArrayList<ReferenceLink> classes;
    @JsonProperty("subclasses")
    private ArrayList<ReferenceLink> subClasses;
}
