package com.example.palantir;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class ConfigurationBeans {

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

}
