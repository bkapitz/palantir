package com.example.palantir.controllers;

import com.example.palantir.models.spells.Spell;
import com.example.palantir.models.spells.SpellList;
import com.example.palantir.service.SpellService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/spells/")
public class Spells {

    @Autowired
    private SpellService spellService;

    @GetMapping("/")
    @ResponseBody
    public SpellList getSpells() {
        return spellService.getSpells();
    }

    @GetMapping("/getDetails/{spellId}")
    @ResponseBody
    public Spell getSpellDetails(@PathVariable String spellId) {
        return spellService.getSpellDetails(spellId);
    }
}
