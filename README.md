# Palantir

This app was started thanks to a push for a code sample for an interview. 
This has long been on a back burner as an idea for an application that would allow a DM in a tabletop game to manage their session and encounters. Currently will only support D&D5E. 



## What it will be

I currently imagine this being a one-stop-shop for a DM. It will allow you to create/manage encounters. It will allow for a combat runner which will put monsters and players in initiative order and help track things like conditions as combat progresses. It will allow be a quick reference for anything a DM might need to know. 

## Features

 - [ ] Database Solution
	 - [ ] Online/Offline
	 - [ ] Update Tables from source
 - [ ] UI Template
 - [ ] Character Manager (lite)
 - [ ] Encounter Manager
 





## UML diagrams

You can render UML diagrams using [Mermaid](https://mermaidjs.github.io/). For example, this will produce a sequence diagram:

```mermaid
sequenceDiagram
Alice ->> Bob: Hello Bob, how are you?
Bob-->>John: How about you John?
Bob--x Alice: I am good thanks!
Bob-x John: I am good thanks!
Note right of John: Bob thinks a long<br/>long time, so long<br/>that the text does<br/>not fit on a row.

Bob-->Alice: Checking with John...
Alice->John: Yes... John, how are you?
```

And this will produce a flow chart:

```mermaid
graph LR
A[Square Rect] -- Link text --> B((Circle))
A --> C(Round Rect)
B --> D{Rhombus}
C --> D
```
