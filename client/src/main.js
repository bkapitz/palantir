import Vue from 'vue'
import App from './App.vue'
import PortalVue from 'portal-vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(PortalVue)
Vue.use(BootstrapVue)
Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  data: {
    currentSort:'name',
    currentSortDir:'asc'
  },
}).$mount('#app');
